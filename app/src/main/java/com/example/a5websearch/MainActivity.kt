package com.example.a5websearch

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val webViewClient = WebViewClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webView.webViewClient = webViewClient
        webView.loadUrl("https://www.google.com")

        // set handler
        searchButton.setOnClickListener(searchButtonHandler)
        searchEditText.setOnEditorActionListener(searchEditTextHandler)
    }

    private var searchButtonHandler = View.OnClickListener { view ->
        val searchText = searchEditText.text.toString()
        search(searchText)

        val ime = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        ime.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private var searchEditTextHandler = TextView.OnEditorActionListener { view, actionId, _ ->
        println("did enter EditTextHandler")
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            val searchText = searchEditText.text.toString()
            search(searchText)
        }
        val ime = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        ime.hideSoftInputFromWindow(view.windowToken, 0)
    }

    @SuppressLint("WrongConstant", "ShowToast")
    private fun search(text: String) {
        if (text.isEmpty()) {
            val toast = Toast.makeText(this, "請輸入內容", 1)
            toast.show()
        } else {
            webView.loadUrl("https://www.google.com.tw/search?q=$text")
        }
    }
}